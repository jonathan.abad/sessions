// JSON Format
// {
//     "city": "Pateros",
//     "province": "Metro Manila",
//     "country": "Philippines"
// }

// JSON Arrays
// "cities": [
//     {
//         "city": "Quezon City",
//         "province": "Metro Manila",
//         "country": "Philippines"
//     },
//     {
//         "city": "Cavite City",
//         "province": "Cavite",
//         "country": "Philippines"
//     },
//     {
//         "city": "Tarlac City",
//         "province": "Tarlac",
//         "country": "Philippines",
//         "rides": [
//             {
//                 "name": "Star Flyer"
//             },
//             {
//                 "name": "Gabi ng Lamig"
//             }
//         ]
//     }
//     ];

// JSON METHODS
let zuitt_batches = [
  {
    batchName: "303",
  },
  {
    batchName: "271",
  },
];

console.log("Output before stringification:");
console.log(zuitt_batches);

// After JSON.stringifiy function, javascript is now reads the variable as string
console.log("Output after stringification:");
console.log(JSON.stringify(zuitt_batches));

// User details

let first_name = prompt("What is your first name?");
let last_name = prompt("What is your last name?");

let other_data = JSON.stringify({
  firstName: first_name,
  lastName: last_name,
});

console.log(other_data);

// Convert Stringified Json into Javascript Objects objects/arrays

let other_data_JSON = `[{ "firstName": "Earl", "lastName": "Diaz" }]`;

let parsed_other_data = JSON.parse(other_data_JSON);
console.log(parsed_other_data);

console.log(parsed_other_data[0].firstName);
