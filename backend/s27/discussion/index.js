// MUTATOR METHODS
// Iteration Methods - loops through all the elements to perform repetitive tasks on the array

// forEach() -  loop through array
// map() - loops through the array and returns a new array
// filter() - returns a new array containing elements which meet the ggiven condition

// every() - checks if all elements meet the given condition
let numbers = [1, 2, 3, 4, 5, 6];
let allValid = numbers.every(function (number) {
  return number > 3;
});

console.log("Result of every() method: ");
console.log(allValid);

// some() - checks if atleast one element meets the given condition
let someValid = numbers.some(function (number) {
  return number < 2;
});

console.log("Result of some() method: ");
console.log(someValid);

// includes() method - can be chained using them one after another

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let filtered_products = products.filter(function (product) {
  return product.toLowerCase().includes("a");
});

console.log(filtered_products);

// reduce ()

let iteration = 0;

let reduced_array = numbers.reduce(function (x, y) {
  console.warn("Current iteration: " + ++iteration);
  console.log("Accumulator :" + x);
  console.log("Current Value :" + y);
  return x + y;
});

console.log("Result of reduce method: " + reduced_array);

let products_reduce = products.reduce(function (x, y) {
  return x + " " + y;
});

console.log("Result of reduce() method: " + products_reduce);
