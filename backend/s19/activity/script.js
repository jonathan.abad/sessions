let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
  houseNumber: "32",
  street: "Washington",
  city: "Lincoln",
  state: "Nebraska",
};

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName); //corrected variable name in console

let currentAge = 40; //corrected the value of the variable
console.log("My current age is: " + currentAge); //corrected the variable name in console

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"]; //put "" on Thor and Natasha
console.log("My Friends are: ");
console.log(friends);

let profile = {
  username: "captain_america",
  fullName: "Steve Rogers", //change the ' to "
  age: 40,
  isActive: false,
};
console.log("My Full Profile: ");
console.log(profile);

let fullName2 = "Bucky Barnes";
console.log("My bestfriend is: " + fullName2); //corrected variable name in console

const lastLocation = "Arctic Ocean";
// lastLocation = "Atlantic Ocean"; // Removed assignment as it is a constant
console.log("I was found frozen in: " + lastLocation);

try {
  module.exports = {
    firstName: typeof firstName !== "undefined" ? firstName : null,
    lastName: typeof lastName !== "undefined" ? lastName : null,
    age: typeof age !== "undefined" ? age : null,
    hobbies: typeof hobbies !== "undefined" ? hobbies : null,
    workAddress: typeof workAddress !== "undefined" ? workAddress : null,
    fullName: typeof fullName !== "undefined" ? fullName : null,
    currentAge: typeof currentAge !== "undefined" ? currentAge : null,
    friends: typeof friends !== "undefined" ? friends : null,
    profile: typeof profile !== "undefined" ? profile : null,
    fullName2: typeof fullName2 !== "undefined" ? fullName2 : null,
    lastLocation: typeof lastLocation !== "undefined" ? lastLocation : null,
  };
} catch (err) {}
