let http = require("http");

const port = 4000;

// MOCK DATABASE
let users = [
  {
    name: "Paulo",
    email: "paulo@email.com",
  },
  {
    name: "Shinji",
    email: "shinji@email.com",
  },
];

const app = http.createServer((req, res) => {
  if (req.url == "/items" && req.method == "GET") {
    res.writeHead(200, { "Content-Type": "text/plain" });

    res.end("Data retrieved from the database");
  }

  //   items POST route

  if (req.url == "/items" && req.method == "POST") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Data sent to the database!");
  }

  //   getting all items from mock database
  if (req.url == "/users" && req.method === "GET") {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify(users));
  }

  //   creating new user in the mock database
  if (req.url == "/users" && req.method === "POST") {
    let request_body = "";
    req.on("data", (data) => {
      request_body += data;
    });
    req.on("end", () => {
      console.log(typeof request_body);
      request_body = JSON.parse(request_body);
      let new_user = {
        name: request_body.name,
        email: request_body.email,
      };
      users.push(new_user);
      res.end(JSON.stringify(new_user));
    });
  }
});
app.listen(port, () => console.log(`Server is running at localhost: ${port}`));
