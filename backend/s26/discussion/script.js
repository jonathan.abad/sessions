// Arrays and Indexes
let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = [
  "Acer",
  "Asus",
  "Lenovo",
  "Neo",
  "Redfox",
  "Gateway",
  "Toshiba",
  "Fujitsu",
  "Lenovo",
];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative way to write arrays
let my_tasks = ["Drink html", "eat javascript", "inhale css", "bake sass"];

// Reassigning values
console.log("Array before reassignment");
console.log(my_tasks);

my_tasks[0] = "run hello world"; // to reassign a value in an array, just use its index number and use an assignment operator to replace the value of that index
console.log("Array after reassignment");
console.log(my_tasks);

// Reading from Arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the length of an array

console.log(computer_brands.length);

// Accessing last element in an array
let index_of_last_element = computer_brands.length - 1;
console.log(computer_brands[index_of_last_element]);

// Array methods/Array Function
// Push Method
let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];
console.log("Current Array: ");
console.log(fruits);

fruits.push("Mango", "Banana");

console.log("Updated array after push method: ");
console.log(fruits);

// Pop Method
console.log("Current Array: ");
console.log(fruits);

let remove_item = fruits.pop();

console.log("Updated array after pop method: ");
console.log(fruits);
console.log("Removed fruit: " + remove_item);

// Unshift Method
console.log("Current Array: ");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method: ");
console.log(fruits);

// Shift Method
console.log("Current Array: ");
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method: ");
console.log(fruits);

// Splice Method
console.log("Current Array: ");
console.log(fruits);

fruits.splice(1, 2, "Lime", "Cherry");

console.log("Updated array after splice method: ");
console.log(fruits);

// Sort Method
console.log("Current Array: ");
console.log(fruits);

fruits.sort();
fruits.reverse();

console.log("Updated array after sort method: ");
console.log(fruits);

// Non mutator Methods - every method written above is called a 'Mutator method'
// because it modifies the value of array one way or another. Non mutator methods on the
// other hand, don't do the same thing, they instead excecute specific functionalities
// that can be done with the existing value of array

let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of lenovo is:" + index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log(
  "The index of lenovo starting from the end of the array is: " +
    index_of_lenovo_from_last_item
);

// Slice Method
let hobbies = ["Gaming", "Running", "Gaslighting", "Cycling", "Writing"];

let sliced_array_from_hobbies = hobbies.slice(2);

console.log(sliced_array_from_hobbies);
console.log(hobbies);

let sliced_array_from_hobbies_b = hobbies.slice(2, 3);

console.log(sliced_array_from_hobbies_b);
console.log(hobbies);

let sliced_array_from_hobbies_c = hobbies.slice(-2);

console.log(sliced_array_from_hobbies_c);
console.log(hobbies);

// toString Method
let string_array = hobbies.toString();
console.log(string_array);

// concat method
let greeting = ["Hello", "World"];
let exclamation = ["!", "?"];
let concat_greeting = greeting.concat(exclamation);

console.log(concat_greeting);

// join Method
console.log(hobbies.join(", "));

// foreach Method

hobbies.forEach(function (hobby) {
  console.log(hobby);
});

// MAP Method
let numbers_list = [1, 2, 3, 4, 5];
let numbers_map = numbers_list.map(function (number) {
  return number * 2;
});

console.log(numbers_map);

// filter Method
let filtered_numbers = numbers_list.filter(function (number) {
  return number < 3;
});

console.log(filtered_numbers);

// Multi-dimensional Arrays
let chess_board = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.log(chess_board[1][4]);
