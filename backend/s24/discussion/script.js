// // While loop
// let count = 5;

// while (count !== 0) {
//   console.log("Current value of count: " + count);
//   count--;
// }

// // Do-while loop
// let number = Number(prompt("Give me a number:"));

// do {
//   console.log("Current value of number: " + number);
//   number += 1;
// } while (number < 10);

// // For loop
// for (let count = 0; count <= 20; count++) {
//   console.log("Current for loop value: " + count);
// }

// let my_string = "earl";
// // to get the lenght of a string
// console.log(my_string.length);

// // to get a specific letter in a string
// console.log(my_string[2]);

// // loops through each letter in the string and will keep iterating as long as the current index is less than the lenght of the string
// for (let index = 0; index < my_string.length; index++) {
//   console.log(my_string[index]); // starts at letter 'e' and will keep printing until 'l'
// }

// Mini activity (20 mins)
// 1. Loop through the 'my_name' variable which has a string with your name on it
// 2. Display each letter in the console but exclude all the vowels from it
// 3. Send a screenshot of the output in our B303 google hangouts
let my_name = "Jonathan Delos Angeles Abad";
const vowels = "aeiou AEIOU";

for (let name = 0; name < my_name.length; name++) {
  let my_name_without_vowels = my_name[name];
  if (vowels.indexOf(my_name_without_vowels) === -1) {
    console.log(my_name_without_vowels);
  }
}

let name_two = "rafael";

for (let index = 0; index < name_two.length; index++) {
  if (name_two[index].toLocaleLowerCase() == "a") {
    console.log("Skipping...");
    continue;
  }
  if (name_two[index] == "e") {
    break;
  }
  console.log(name_two[index]);
}
