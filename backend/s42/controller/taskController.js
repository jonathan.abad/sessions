const Task = require("../models/Task");

module.exports.getAllTasks = (request, response) => {
  Task.find({}).then((result, error) => {
    if (error) {
      return response.status(500).json({
        message: error.message,
      });
    }
    return response.status(200).json({
      tasks: result,
    });
  });
};

module.exports.createTask = (request, response) => {
  Task.findOne({ name: request.body.name }).then((result, error) => {
    if (error) {
      return response.status(500).json({
        message: error.message,
      });
    }
    if (result != null && result.name == request.body.name) {
      return response.status(400).json({
        message: "Duplicate task found!",
      });
    } else {
      // Create a new instance of the task model with the required properties based on the schema
      let newTask = new Task({
        name: request.body.name,
      });
      newTask.save().then((savedTask, error) => {
        if (error) {
          return response.status(500).json({
            message: error.message,
          });
        }
        return response.status(201).json({
          message: "New task created!",
          task: savedTask,
        });
      });
    }
  });
};
