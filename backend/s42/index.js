// Server variables
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes");
const app = express();
const port = 4000;

// MongoDB connection
mongoose.connect(
  `mongodb+srv://admin:admin1234@303-abad.h42yc3b.mongodb.net/b303-todo?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
let database = mongoose.connection;

database.on("error", () => console.log("Connection Error :"));
database.once("open", () => console.log("Connected to MongoDB"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(taskRoutes);

// Server Listening
app.listen(port, () => console.log(`Server is running at ${port}`));

module.exports = app;
