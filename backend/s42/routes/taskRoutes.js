const express = require("express");
const router = express.Router();
const taskController = require("../controller/taskController");

// Insert routes here
router.post("/tasks", taskController.createTask);
router.get("/tasks", taskController.getAllTasks);

module.exports = router;
