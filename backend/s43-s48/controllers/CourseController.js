const Course = require("../models/Course");

module.exports.addCourse = (request_body) => {
  let new_course = new Course({
    name: request_body.name,
    description: request_body.description,
    price: parseInt(request_body.price),
  });
  return new_course
    .save()
    .then((registered_course, error) => {
      if (error) {
        return {
          message: error.message,
        };
      }
      return {
        course: registered_course,
      };
    })
    .catch((error) => console.log(error));
};

module.exports.getAllCourses = (request, response) => {
  return Course.find({}).then((result) => {
    return response.send(result);
  });
};

module.exports.getAllActiveCourses = (request, response) => {
  return Course.find({ isActive: true }).then((result) => {
    return response.send(result);
  });
};

module.exports.getCourse = (request, response) => {
  return Course.findById(request.params.id).then((result) => {
    return response.send(result);
  });
};

module.exports.updateCourse = (request, response) => {
  let updatedCourseDetails = {
    name: request.body.name,
    description: request.body.description,
    price: parseInt(request.body.price),
  };
  return Course.findByIdAndUpdate(request.params.id, updatedCourseDetails)
    .then((course, error) => {
      if (error) {
        return response.send({
          message: error.message,
        });
      }
      return response.send({
        message: "Course has been updated successfully!",
        course: course,
      });
    })
    .catch((error) => console.log(error));
};

module.exports.archiveCourse = (request, response) => {
  let deactivatedCourse = {
    isActive: false,
  };
  return Course.findByIdAndUpdate(request.params.id, deactivatedCourse)
    .then((course, error) => {
      if (error) {
        return resopnse.send({
          message: error.message,
        });
      }
      return response.send({
        message: "Course has been deactivated",
        isActive: false,
      });
    })
    .catch((error) => console.log(error));
};

module.exports.activateCourse = (request, response) => {
  let activatedCourse = {
    isActive: true,
  };
  return Course.findByIdAndUpdate(request.params.id, activatedCourse)
    .then((course, error) => {
      if (error) {
        return resopnse.send({
          message: error.message,
        });
      }
      return response.send({
        message: "Course has been activated",
      });
    })
    .catch((error) => console.log(error));
};

module.exports.searchCourses = (request, response) => {
  const courseName = request.body.courseName;

  return Course.find({ name: { $regex: courseName, $options: "i" } })
    .then((courses) => {
      response.send(courses);
    })
    .catch((error) =>
      response.send({
        message: error.message,
      })
    );
};
