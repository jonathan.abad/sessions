const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check email
module.exports.checkEmailExists = (request_body) => {
  return User.find({ email: request_body.email }).then((result, error) => {
    if (error) {
      return {
        message: error.message,
      };
    }
    if (result.length <= 0) {
      return false;
    }
    // this will only return true if there are no errors and there is a result from the query
    return true;
  });
};

// Details
module.exports.getProfile = (request_body) => {
  return User.findById({ _id: request_body.id })
    .then((result, error) => {
      if (error) {
        return {
          message: error.message,
        };
      }
      if (!result) {
        return response.status(404).json({ message: "User not found" });
      }
      result.password = "";
      return {
        users: result,
      };
    })
    .catch((error) => console.log(error));
};

// Register
module.exports.registerUser = (request_body) => {
  let new_user = new User({
    firstName: request_body.firstName,
    lastName: request_body.lastName,
    email: request_body.email,
    mobileNo: request_body.mobileNo,
    password: bcrypt.hashSync(request_body.password, 10),
  });
  return new_user
    .save()
    .then((registered_user, error) => {
      if (error) {
        return {
          message: error.message,
        };
      }
      return {
        message: "Successfully registered a user!",
        data: registered_user,
      };
    })
    .catch((error) => console.log(error));
};
// Login
module.exports.loginUser = (request, response) => {
  return User.findOne({ email: request.body.email })
    .then((result) => {
      if (result == null) {
        return response.send({
          message: "The user isn't registered yet",
        });
      }
      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        result.password
      );
      if (isPasswordCorrect) {
        return response.send({ accessToken: auth.createAccessToken(result) });
      } else {
        return response.send({
          message: "Your password is incorrect",
        });
      }
    })
    .catch((error) => response.send(error));
};

module.exports.enroll = async (request, response) => {
  //blocks this function if the user is an admin
  if (request.user.isAdmin) {
    return response.send("Action Forbidden");
  }
  // this variable will return true once the user data has been updated
  let isUserUpdated = await User.findById(request.user.id).then((user) => {
    let newEnrollment = {
      courseId: request.body.courseId,
      courseName: request.body.courseName,
      courseDescription: request.body.courseDescription,
      coursePrice: request.body.coursePrice,
    };

    user.enrollments.push(newEnrollment);

    return user
      .save()
      .then((updateUser) => true)
      .catch((error) => error.message);
  });

  if (isUserUpdated !== true) {
    return response.send({
      message: isUserUpdated,
    });
  }

  let isCourseUpdated = await Course.findById(request.body.courseId).then(
    (course) => {
      let newEnrollee = {
        userId: request.user.id,
      };
      course.enrollees.push(newEnrollee);

      return course
        .save()
        .then((updatedCourse) => true)
        .catch((error) => error.message);
    }
  );
  if (isCourseUpdated !== true) {
    return response.send({
      message: isCourseUpdated,
    });
  }

  if (isUserUpdated && isCourseUpdated) {
    return response.send({
      message: "Enrolled Successfully",
    });
  }
};

module.exports.getEnrollment = (request, response) => {
  User.findById(request.user.id)
    .then((result) => response.send(result.enrollments))
    .catch((error) => response.send(error.message));
};
