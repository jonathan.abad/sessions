// Server variables
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();
const port = 4000;
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes.js");
const app = express();

// Middleware

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors()); //this will allow our hosted frontend app to send request to this server

// Routes
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

// MongoDB connection
mongoose.connect(
  `mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-abad.h42yc3b.mongodb.net/b303-booking-api?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
let database = mongoose.connection;

database.on("error", () => console.log("Can't connect to database :"));
database.once("open", () => console.log("Connected to MongoDB"));

// Server Listening
app.listen(process.env.PORT || port, () => {
  console.log(
    `Booking System API is now running at localhost: ${
      process.env.PORT || port
    }`
  );
});

module.exports = app;
