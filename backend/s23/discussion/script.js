// if-else statements
let number = 1;

if (number > 1) {
  console.log("The number is greater that 1");
} else if (number < 1) {
  console.log("The number is less than 1");
} else {
  console.log("None of the conditions were true");
}

// Falsey Values
if (false) {
  console.log("Falsey");
}
if (0) {
  console.log("Falsey");
}
if (undefined) {
  console.log("Falsey");
}

//Truthy Values
if (true) {
  console.log("Truthy");
}
if (1) {
  console.log("Truthy");
}
if ([]) {
  console.log("Truthy");
}

//Ternary Operators
let result = 1 < 10 ? true : false;

// if there are multiple lines within the if-else block,
//it's better to use the regular if-else syntax as the ternary operator is only capable of handling one-liners
if (5 == 5) {
  let greeting = "Hello";
  console.log(greeting);
}

console.log("Value returned from the ternary operator " + result);

//Switch Statement

let day = prompt("What day of the week is it today?").toLocaleLowerCase();

switch (day) {
  case "monday":
    console.log("The day today is monday!");
    break;
  case "tuesday":
    console.log("The day today is tuesday!");
    break;
  case "wednesday":
    console.log("The day today is wednesday!");
    break;
  case "thursday":
    console.log("The day today is thursday!");
    break;
  case "friday":
    console.log("The day today is friday!");
    break;
  default:
    console.log("Please input a valid day naman paareh");
    break;
}

// Try/Catch/Finally Statements
function showIntensityAlert(windSpeed) {
  try {
    alerat(determineTyphoonIntensity(windSpeed));
  } catch (error) {
    console.log(error.message);
  } finally {
    alert("Intensity updates will show new alert");
  }
}
showIntensityAlert(56);
