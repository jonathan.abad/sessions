// console.log("ES6 Updates");

// Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
/*  allows us to write strings w/o using concatenation
    greatly helps us with code readability
*/

let name = "Ken";

//  Using concatenation

let message = "Hello " + name + "! Welcome to Programming";
console.log("Message without template literals: " + message);

// using template literals

message = `Hello ${name}! Welcome to Programming`;
console.log(`Message with template literals:  ${message}`);

// Creates multy line using templates literals
const anotherMessage = `
${name} attended a Math Competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;

console.log(
  `The interest on your savings amount is: ${principal * interestRate}`
);

// Array Destructuring
const fullName = ["Juan", "Dela", "Cruz"];

// using array indeces
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`
);

// using array destructuring

const [firstName, middleName, lastName] = fullName;

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`
);

// object Destructuring

const person = {
  givenName: "Jane",
  maidenName: "Dela",
  surName: "Cruz",
};

// using object destructuring
const { givenName, maidenName, surName } = person;
console.log(
  `Hello ${givenName}, ${maidenName}. ${surName}! It's good to see you!`
);

// using dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.surName);

console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.surName}! It's good to see you!`
);
// using object destructuring in functions
function getFullName({ givenName, maidenName, surName }) {
  console.log(`${givenName} ${maidenName} ${surName}`);
}

getFullName(person);

// Arrow Functions
const hello = () => {
  console.log("Hello World!");
};

hello();

// Traditional Functions
// function printFullName(fName, mName, lName) {
//   console.log(fName + " " + mName + " " + lName);
// }

// printFullName("John", "D.", "Smith");

// Arrow function with template literals
const printFullName = (fName, mName, lName) => {
  console.log(`${fName} ${mName} ${lName}`);
};
printFullName("Jane", "D.", "Smith");

// Arrow Functions with loops
const students = ["John", "Jane", "Judy"];

// Traditional Function
students.forEach(function (student) {
  console.log(`${student} is a student`);
});

students.forEach((student) => {
  console.log(`${student} is a student`);
});

// Implicit Return Statements
// Traditional Functions

// function add(x, y) {
//   return x + y;
// }

// let total = add(2, 5);
// console.log(total);

// Arrow function
const add = (x, y) => x + y;
let total = add(2, 5);
console.log(total);

// Default Argument Values
const greet = (name = "User") => {
  return `Good afternoon ${name}`;
};

console.log(greet());
console.log(greet("Judy"));

// Cass-based Object Blueprints

// Create a class

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

// Instantiate an object
const fordExplorer = new Car();
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

const toyotaVios = new Car("Toyota", "Vios", 2018);
console.log(toyotaVios);
