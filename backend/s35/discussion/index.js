// db.users.insertMany([
//   {
//     firstName: "Jane",
//     lastName: "Doe",
//     age: 21,
//     contact: {
//       phone: "87654321",
//       email: "janedoe@gmail.com",
//     },
//     courses: ["CSS", "Javascript", "Python"],
//     department: "none",
//   },
//   {
//     firstName: "Stephen",
//     lastName: "Hawking",
//     age: 76,
//     contact: {
//       phone: "87654321",
//       email: "stephenhawking@gmail.com",
//     },
//     courses: ["Python", "React", "PHP"],
//     department: "none",
//   },
//   {
//     firstName: "Neil",
//     lastName: "Armstrong",
//     age: 82,
//     contact: {
//       phone: "87654321",
//       email: "neilarmstrong@gmail.com",
//     },
//     courses: ["React", "Laravel", "Sass"],
//     department: "none",
//   },
// ]);

// Greater than operator
db.users.find({
  age: {
    $gt: 20,
  },
});



//Less than operator
db.users.find({
  age: {
    $lte: 82,
  },
});

// Regex operator
db.users.find({
  firstName: {
    $regex: "s",
    $options: "i",
  },
});

db.users.find({
  lastName: {
    $regex: "T",
    $options: "i",
  },
});

// Combing operators
db.users.find({
  age: {
    $gt: 70,
  },
  lastName: {
    $regex: "g",
  },
});

db.users.find({
  age: {
    $lte: 76,
  },
  firstName: {
    $regex: "j",
    $options: "i",
  },
});

// Field projection
db.users.find(
  {},
  {
    _id: 0,
  }
);

db.users.find(
  {},
  {
    firstName: 1,
    _id: 0, // exclude the id field from results
  }
);
