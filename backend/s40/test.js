const express = require("express");
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, () => console.log(`Server is running at port ${port}`));

app.get("/home", (request, response) => {
  response.send("Welcome to the home page");
});

let user = [
  {
    username: "johndoe",
    password: "johndoe1234",
  },
  {
    username: "johndoe1",
    password: "johndoe12345",
  },
];

app.get("/users", (request, response) => {
  response.send(user);
});

app.delete("/delete-user", (request, response) => {
  let message;

  if (user.length !== 0) {
    for (let i = 0; i < user.length; i++) {
      if (request.body.username === user[i].username) {                                                                                                                                                                          
        user.splice(i, 1);

        message = `User ${request.body.username} has been deleted`;
        break;
      }
    }

    if (message === undefined) {
      message = "User does not exist.";
    }
  } else {
    message = "No users found.";
  }
  response.send(message);
});
