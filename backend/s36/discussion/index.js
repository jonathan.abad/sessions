// aggregate method
// $match i used to match or get documents that satisfies the condition
// it is similar to find(). you can use query operators to make your criteria more flexible
db.fruits.aggregate([
  { $match: { onSale: true } }, // $match -> Apple, Kiwi, Banana
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }, //Apple = 1.0, Kiwi = 1.0, Banana = 2.0
  //$group allows us to group together documents and create an analysis oput of the gourp elements
]);

db.fruits.aggregate([
  /*
    $match - Apple, Kiwi, Banana
    */

  { $match: { onSale: true } },
  /*
  Apple = 1.0
  Kiwi = 1.0
  Banana = 2.0

  id = 1.0
    avg stock = 22.5

  id = 2.0
    avg stock = 15
   */
  { $group: { _id: "$supplier_id", avgStock: { $avg: "$stock" } } },
  /*
  hide the _id
   */
  { $project: { _id: 0 } },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } },
  { $sort: { maxPrice: -1 } },
]);

db.fruits.aggregate([{ $unwind: "$origin" }]);

db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
]);
