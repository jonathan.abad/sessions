// Server variables
const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config(); //Initialization of dotenv package
const app = express();
const port = 4000;

// MongoDB connection
mongoose.connect(
  `mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-abad.h42yc3b.mongodb.net/b303-todo?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
let database = mongoose.connection;

database.on("error", () => console.log("Connection Error :"));
database.once("open", () => console.log("Connected to MongoDB"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Mongoose Schema
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending",
  },
});

// Models
const Task = mongoose.model("Task", taskSchema);

// Routes
app.post("/tasks", (request, response) => {
  Task.findOne({ name: request.body.name }).then((result, error) => {
    if (result != null && result.name == request.body.name) {
      return response.send("Duplicate task found!");
    } else {
      // Create new instance of the task model which will contait that properties required based on the schema
      let newTask = new Task({
        name: request.body.name,
      });
      newTask.save().then((saveTask, error) => {
        if (error) {
          return response.send({
            message: error.message,
          });
        }
        return response.send(201, "New task created!");
      });
    }
  });
});

app.get("/tasks", (request, response) => {
  Task.find({}).then((result, error) => {
    if (error) {
      return response.send({
        message: error.message,
      });
    }
    return response.status(200).json({
      tasks: result,
    });
  });
});

// Server Listening
app.listen(port, () => console.log(`Server is running at ${port}`));

module.exports = app;
