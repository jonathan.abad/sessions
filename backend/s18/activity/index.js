function scrollToSection(sectionId) {
  const section = document.getElementById(sectionId);
  if (section) {
    section.scrollIntoView({ behavior: "smooth" });
  }
}

const contactForm = document.getElementById("contact-form");
const sendAnotherButton = document.getElementById("send-another-button");

contactForm.addEventListener("submit", function (event) {
  event.preventDefault();
  // Perform form submission logic here

  // Hide the form
  contactForm.style.display = "none";

  // Show the "Send another message" button
  sendAnotherButton.style.display = "block";
});

sendAnotherButton.addEventListener("click", function () {
  // Show the form
  contactForm.style.display = "block";

  // Hide the "Send another message" button
  sendAnotherButton.style.display = "none";
});
