// Arguments and Parameters
function printName(name) {
  console.log("I'm the real " + name);
}

printName("Slim Shady");

function checkDivisibilityBy2(number) {
  let result = number % 2;

  console.log("The remainder of " + number + " is " + result);
}

checkDivisibilityBy2(15);

// Multiple Arguments and Parameters
function createFullName(firstName, middleName, lastName) {
  console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");

// Usage of Prompts and Alerts
let user_name = prompt("Enter your username: ");

function displayWelcomeMessageForUser(user_name) {
  alert("Welcome back to Valorant " + user_name);
}

// displayWelcomeMessageForUser("68lover");

displayWelcomeMessageForUser(user_name);
