// console.log("love, objects");

// objects

// creating objects
let cellphone = {
  name: "Nokia 3210",
  manufactureDate: 1999,
};

console.log("Result from creating objects using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);

// creating objects using a constructor funnction
function Laptop(name, manufactureDate) {
  this.name = name;
  this.manufactureDate = manufactureDate;
}
// multiple instance of an object using the new keyword this methdo is called instantiation
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using constructor function");
console.log(laptop);

let laptop2 = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using constructor function");
console.log(laptop2);

// Accessing Object Properties

// using square bracket notation
console.log("Resulting from square bracket notation: " + laptop2["name"]);

// using dot notation
console.log("Result from dot notation: " + laptop2.name);

// Access array objects
let array = [laptop, laptop2];

console.log(array[0]["name"]);
console.log(array[0].name);

// Adding/Deleting/Reassigning Object properties

let car = {};
// empty object literal initialization for now

// adding object properties

car.name = "Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);

// adding using square bracket notation
car["manufacturing date"] = 2019;
console.log(car["manufacturing_date"]);
console.log(car["Manufacturing Date"]);
// console.log(car.manufacturing_date);

console.log("REsult from adding propertie using square bracket notation: ");
console.log(car);

// Deleting object propeties
delete car["manufacturing date"];
console.log("Result from deleting properties: ");
console.log(car);

// reassigning object properties
car.name = "Honda Civic Type R";
console.log("Resulting from reassigning properties: ");
console.log(car);

// Object methods
//a method is a funtion which a  property of an object

let person = {
  name: "Barbie",
  greet: function () {
    console.log("Hello! My name is " + this.name);
  },
};
console.log(person);
console.log("Result from object methods: ");
person.greet();

// adding methods to objects
person.walk = function () {
  console.log(this.name + " walked 25 steps forward");
};

person.walk();

let friend = {
  name: "Ken",
  address: {
    city: "Austin",
    state: "Texas",
    country: "USA",
  },
  email: ["ken@gmail.com", "ken@email.com"],
  introduce: function (person) {
    console.log(
      "Nice to meet you " +
        person.name +
        " Iam " +
        this.name +
        "from " +
        this.address.city +
        " " +
        this.address.state
    );
  },
};

friend.introduce(person);
