let collection = [];

function print() {
  return collection;
}

function enqueue(item) {
  let newCollection = [];
  for (let i = 0; i < collection.length; i++) {
    newCollection[i] = collection[i];
  }
  newCollection[collection.length] = item;
  collection = newCollection;
  return collection;
}

function dequeue() {
  if (collection.length === 0) {
    return [];
  }
  let newCollection = [];
  for (let i = 1; i < collection.length; i++) {
    newCollection[i - 1] = collection[i];
  }
  collection = newCollection;
  return collection;
}

function front() {
  return collection.length > 0 ? collection[0] : undefined;
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
