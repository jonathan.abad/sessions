import React, { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext.js";
import AdminView from "../components/AdminView.js";
import UserView from "../components/UserView.js";
import CourseSearchByPrice from "../components/CourseSearchByPrice.js";

export default function Courses() {
  const { user } = useContext(UserContext);
  const [courses, setCourses] = useState([]);

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setCourses(data);
      });
  };

  const searchByPrice = (minPrice, maxPrice) => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ minPrice, maxPrice }),
    })
      .then((res) => res.json())
      .then((data) => {
        setCourses(data.courses);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      {user.isAdmin === true ? (
        <AdminView coursesData={courses} fetchData={fetchData} />
      ) : (
        <>
          <CourseSearchByPrice onSearch={searchByPrice} />
          <UserView coursesData={courses} />
        </>
      )}
    </div>
  );
}
