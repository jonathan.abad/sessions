import Banner from "../components/Banner.js";
import FeaturedCourses from "../components/FeaturedCourses.js";
import Highlights from "../components/Highlights.js";
import { Link } from "react-router-dom";
export default function Home() {
  return (
    <>
      <Banner
        title="
		Zuitt Coding Bootcamp"
        subtitle="Opportunities for everyone, everywhere"
        buttonText={
          <Link to="/courses" className="text-light">
            Enroll Now
          </Link>
        }
      />
      <FeaturedCourses />
      <Highlights />
    </>
  );
}
