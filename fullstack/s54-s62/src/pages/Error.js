import Banner from "../components/Banner.js";
import { Link } from "react-router-dom";
export default function Error() {
  return (
    <Banner
      title="404 - Not found"
      subtitle="The page you're looking for cannot be found."
      buttonText={
        <Link to="/" className="text-light">
          Back Home
        </Link>
      }
    />
  );
}
