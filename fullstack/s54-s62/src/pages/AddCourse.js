import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { useNavigate, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext.js";

export default function AddCourse() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [isActive, setIsActive] = useState(false);

  function addCourse(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Course Added",
            icon: "success",
          });
          navigate("/courses");
        } else {
          Swal.fire({
            title: "Unsuccessful Course Creation",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
    setName("");
    setDescription("");
    setPrice("");
  }

  useEffect(() => {
    if (name !== "" && description !== "" && price !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  return user.id === null || user.isAdmin === false ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(event) => addCourse(event)}>
      <h1 className="my-5 text-center">Add Course</h1>
      <Form.Group>
        <Form.Label>Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Name"
          required
          value={name}
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Description:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Description"
          required
          value={description}
          onChange={(event) => {
            setDescription(event.target.value);
          }}
        />
      </Form.Group>
      <Form.Group className="mb-4">
        <Form.Label>Price:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Price"
          required
          value={price}
          onChange={(event) => {
            setPrice(event.target.value);
          }}
        />
      </Form.Group>
      <Button variant="primary" type="submit" disabled={isActive === false}>
        Submit
      </Button>
    </Form>
  );
}
