import { useState, useEffect } from "react";
import CourseCard from "./CourseCard";
import CourseSearch from "./CourseSearch";

export default function UserView({ coursesData }) {
  const [courses, setCourses] = useState([]);

  const activeCourses = courses.filter((course) => course.isActive);
  const courseCards = activeCourses.map((course) => (
    <CourseCard key={course.id} course={course} />
  ));

  useEffect(() => {
    // Update the state with the coursesData prop
    setCourses(coursesData);
  }, [coursesData]);

  return (
    <div className="mt-2">
      <CourseSearch />
      {courseCards}
    </div>
  );
}
