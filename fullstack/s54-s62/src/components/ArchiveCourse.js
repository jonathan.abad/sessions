import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function ArchiveCourse({ courseId, isActive, fetchData }) {
  const handleArchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Archived!",
            icon: "success",
            text: "Course archived successfully",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "An error occurred while activating the course.",
          });
        }
      });
  };

  const handleActivate = () => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Activated!",
            icon: "success",
            text: "Course activated successfully",
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: "An error occurred while activating the course.",
          });
        }
      });
  };

  return (
    <>
      {isActive ? (
        <Button className="btn btn-danger" onClick={handleArchive}>
          Archive
        </Button>
      ) : (
        <Button className="btn btn-success" onClick={handleActivate}>
          Activate
        </Button>
      )}
    </>
  );
}
