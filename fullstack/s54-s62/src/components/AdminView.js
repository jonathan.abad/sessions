import { useState, useEffect } from "react";

import { Row, Col, Table } from "react-bootstrap";
import EditCourse from "./EditCourse";
import ArchiveCourse from "./ArchiveCourse";

export default function AdminView({ coursesData, fetchData }) {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    setCourses(coursesData);
  }, [coursesData]);

  const courseData = courses.map((course) => (
    <tr key={course._id}>
      <td>{course._id}</td>
      <td>{course.name}</td>
      <td>{course.description}</td>
      <td>{course.price}</td>
      <td>
        {course.isActive ? (
          <span className="text-success">Available</span>
        ) : (
          <span className="text-danger">Unavailable</span>
        )}
      </td>
      <td>
        {/* to={`/${course.id}/edit`} */}
        <EditCourse course={course._id} fetchData={fetchData} />
      </td>
      <td>
        <ArchiveCourse
          courseId={course._id}
          isActive={course.isActive}
          fetchData={fetchData}
        />
        {/* to={`/${course.id}/archive`} */}
      </td>
    </tr>
  ));

  return (
    <Row>
      <Col>
        <h3 className="text-center mt-4">Admin Dashboard</h3>
        <Table striped bordered hover responsive>
          <thead>
            <tr className="text-center">
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Availability</th>
              <th colSpan={2}>Action</th>
            </tr>
          </thead>
          <tbody>{courseData}</tbody>
        </Table>
      </Col>
    </Row>
  );
}
