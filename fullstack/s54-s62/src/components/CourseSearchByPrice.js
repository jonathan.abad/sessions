import React, { useState } from "react";

const CourseSearchByPrice = ({ onSearch }) => {
  const [minPrice, setMinPrice] = useState("");
  const [maxPrice, setMaxPrice] = useState("");

  return (
    <div className="row mt-4">
      <div className="col-md-4">
        <label>Min Price:</label>
        <input
          type="number"
          className="form-control"
          value={minPrice}
          onChange={(e) => setMinPrice(e.target.value)}
        />
      </div>
      <div className="col-md-4">
        <label>Max Price:</label>
        <input
          type="number"
          className="form-control"
          value={maxPrice}
          onChange={(e) => setMaxPrice(e.target.value)}
        />
      </div>
      <div className="col-md-4">
        <button
          onClick={() => onSearch(minPrice, maxPrice)}
          className="btn btn-primary mt-4"
        >
          Search
        </button>
      </div>
    </div>
  );
};

export default CourseSearchByPrice;
