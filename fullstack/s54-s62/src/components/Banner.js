import { Button, Row, Col } from "react-bootstrap";

export default function Banner({ title, subtitle, buttonText }) {
  return (
    <Row>
      <Col className="p-5 text-center">
        <h1>{title}</h1>
        <p>{subtitle}</p>
        <Button variant="primary">{buttonText}</Button>
      </Col>
    </Row>
  );
}
