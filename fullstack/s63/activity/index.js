function countLetter(letter, sentence) {
  let result = 0;
  // If letter is invalid, return undefined.
  // Check first whether the letter is a single character.

  // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.

  // check if letter is single character and if letter is in string
  if (typeof letter !== "string" || letter.length !== 1) {
    return undefined;
  }

  let count = 0;
  //iterate through the sentence
  for (let i = 0; i < sentence.length; i++) {
    // check the letter in the index and compare to the letter
    if (sentence[i] === letter) {
      count++; //increment count if letter have a match
    }
  }
  return count;
}

function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  // If the function finds a repeating letter, return false. Otherwise, return true.

  //convert text to lowercase
  text = text.toLowerCase();

  //iterate through each letter in the text
  for (let i = 0; i < text.length; i++) {
    for (let j = i + 1; j < text.length; j++) {
      if (text[i] === text[j]) {
        //compare each letter in the text
        return false;
      }
    }
  }
  return true;
}

function purchase(age, price) {
  // Return undefined for people aged below 13.
  // Return the discounted price (20% discount) for students aged 13 to 21.
  // Return the discounted price (20% discount) for senior citizens.
  // Return the rounded off price for people aged 22 to 64.
  // The returned value should be a string.

  // Check if the age is less than 13
  if (age < 13) {
    return undefined;
  }

  // Calculate the discounted price
  if (age >= 13 && age <= 21) {
    price = price * 0.8; // Apply 20% discount
  } else if (age >= 60) {
    price = price * 0.8; // Apply 20% discount for senior citizens
  }

  if (age >= 22 && age <= 64) {
    price = Math.round(price * 100) / 100; // Round to 2 decimal places
    return price;
  }
  return price.toFixed(2);
}

console.log(purchase(34, 109.4356));

function findHotCategories(items) {
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.
  // The passed items array from the test are the following:
  // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
  // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
  // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
  // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
  // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

  //create new array set
  const hotCategories = new Set();
  //iterate through items and compare if item stocks if = 0
  for (const item of items) {
    if (item.stocks === 0) {
      hotCategories.add(item.category); // if comparing matched, add the category in the new array
    }
  }
  //   return the new array
  return Array.from(hotCategories);
}

function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.
  // The passed values from the test are the following:
  // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
  // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
  // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

  //create new array set
  const voters = new Set();
  //iterate through candidate A
  for (const candidate1 of candidateA) {
    // iterate through candidate B
    for (const candidate2 of candidateB) {
      // if voter in candidate A and candidate B matched, add the voter in the new array
      if (candidate1 === candidate2) {
        voters.add(candidate1);
      }
    }
  }
  return Array.from(voters);
}

module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters,
};
